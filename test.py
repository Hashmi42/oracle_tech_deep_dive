import os
import sys
import unittest
from models import db,OracleCloudApp

class DBTestCases(unittest.TestCase):
    def test_source_insert_1(self):
        s = OracleCloudApp(id='25',name='test1',description='This is a test',fact1='test fact1',fact2='test fact2',fact3='empty test fact')
        db.session.add(s)
        db.session.commit()
        r = db.session.query(OracleCloudApp).filter_by(id = '25').one()
        self.assertEqual(str(r.id), '25')
        db.session.query(OracleCloudApp).filter_by(id = '25').delete()
        db.session.commit()

if __name__ == '__main__':
    unittest.main()
