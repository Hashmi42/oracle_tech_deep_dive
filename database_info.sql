\c oracleInterview;

--
set client_min_messages=warning;
drop table if exists OracleCloud;


--

create table OracleCloud (
  id int,
  name text,
  description text,
  fact1 text,
  fact2 text,
  fact3 text);

--
insert into OracleCloud values (1,'Fact One','Cloud mainly has three Service Models.','Software as a Service (SaaS)','Infrastructure as a Service (IaaS)','Platform as a Service (PaaS)');
insert into OracleCloud values (2,'Fact Two','Under IaaS, Oracle has two main offerings (OCI & OCI-C). For EBS (R12) you can also have dedicate compute on Sparc Model','Oracle Cloud Infrastructure (OCI) Compute — This was earlier Bare Metal Cloud Service (BMCS)',
  'Oracle Cloud Infrastructure Classic (OCI-C) Compute (Compute Classic)''Oracle Cloud Infrastructure Compute Classic – Dedicated Compute Capacity – SPARC Model 300');
insert into OracleCloud values (3,'Fact Three','Oracle Database Cloud Service (DBCS) offering is from PaaS and has two main common options (apart from Schema as a Service)','Database as a Service (DBaaS)','Exadata Cloud Service (ExaCS)','Empty');
insert into OracleCloud values (4,'Fact Four','Oracle EBS (R12) has two main tiers (Third is Client Tier i.e.Java-enabled Browser)','Database Tier: Oracle EBS Database is deployed in this tier','Application Tier: Oracle EBS Middle Tier','Empty');
insert into OracleCloud values (5,'Fact Five','You can deploy EBS on a cloud in three ways:','Traditional Deployment: In a traditional deployment, we deploy everything on customer hardware.',
  'Hybrid Deployment: In a hybrid deployment, we deploy Apps tier on Customer hardware and database tier on Cloud.','Oracle Public Cloud: In this deployment, apps tier will be deployed on IaaS and Database tier can be deployed on IaaS or PaaS (DBCS or Exadata CS).');
insert into OracleCloud values (6,'Fact Six','EBS (R12) Application Tier must be deployed on IaaS and is supported on all three IaaS offerings from Oracle (OCI Compute, OCI Compute Classic, OCI Compute Classic SPARC)','Empty','Empty','Empty');
insert into OracleCloud values (7,'Fact Seven','EBS R12 (Database) can be deployed on either IaaS or PaaS (DBCS offering). EBS R12 Database Tier on PaaS can be configured with either DBaaS or ExaCS','Empty','Empty','Empty');
insert into OracleCloud values (8,'Fact Eight','Oracle Database Cloud Service (DBCS) has four options for Software Editions','Standard Edition (SE)','Enterprise Edition (EE)','Enterprise Edition High Performance (EE-HP)');
insert into OracleCloud values (9,'Fact Nine','If Oracle EBS (R12) Database Tier is using Oracle Database Cloud Service, then Software Edition should be either EE-HP or EE-EP','Empty','Empty','Empty');
insert into OracleCloud values (10,'Fact Ten','If EBS (R12) Application Tier is on IaaS and Database Tier is on PaaS (DBCS or ExaCS) then these two services must be co-located in same Identity Domain.','Empty','Empty','Empty');
insert into OracleCloud values (11,'Fact Eleven','You can run Oracle EBS(R12) with RAC on Oracle Cloud but in that case Database Tier must be on PaaS Offering (DBCS or ExaCS)','Empty','Empty','Empty');
insert into OracleCloud values (12,'Fact Twelve','You can create new Oracle EBS (R12) – Vision or Fresh Database using','Template from Oracle Cloud Marketplace','Using EBS Cloud Manager tool.''Empty');
insert into OracleCloud values (13,'Fact Thirteen','You can Lift & Shift (Migrate) an On-Premise EBS (R12) Instance to Oracle Cloud using Automated Method.','Empty','Empty','Empty');
insert into OracleCloud values (14,'Fact Fourteen','Oracle Cloud at Customer (OCC) is service where Customer can keep Oracle Cloud Machines (On Subscription Basis) at their own data centers and run EBS on Cloud similar to Oracle Public Cloud.','Empty','Empty','Empty');
insert into OracleCloud values (15,'Fact Fifteen','Oracle EBS R12 is NOT certified on Amazon AWS (IaaS)','Empty','Empty','Empty');

\quit
