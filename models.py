from flask import Flask, render_template
from flask_sqlalchemy import SQLAlchemy
import os

app = Flask(__name__,template_folder='templates')
app.config['SQLALCHEMY_DATABASE_URI'] = os.environ.get("DB_STRING",'postgres://postgres:password@localhost:5432/oracleInterview')
db = SQLAlchemy(app)

class OracleCloudApp(db.Model):
    __tablename__ = "oraclecloud"
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String())
    description = db.Column(db.String())
    fact1 = db.Column(db.String())
    fact2 = db.Column(db.String())
    fact3 = db.Column(db.String())

    def __init__(self,id,name,description,fact1,fact2,fact3):
        self.id = id
        self.name = name
        self.description = description
        self.fact1 = fact1
        self.fact2 = fact2
        self.fact3 = fact3

db.create_all()
