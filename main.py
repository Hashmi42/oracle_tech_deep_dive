#!/usr/bin/env python

# -----------------------------------------
# main.py
# flask application
# -----------------------------------------
#
#
from flask import Flask, render_template
from models import app, db, OracleCloudApp
import shlex, subprocess


@app.route('/index')
def index():
    return render_template('index.html')

@app.route('/about')
def about():
    return render_template('about.html')

@app.route('/oracleApp')
def oracleApp():
    cloud_db = db.session.query(OracleCloudApp).all()
    return render_template('oracleApps.html',cloud_db=cloud_db)

@app.route('/test/')
def test():
	p = subprocess.Popen(["coverage", "run", "--branch", "test.py"],
			stdout=subprocess.PIPE,
			stderr=subprocess.PIPE,
			stdin=subprocess.PIPE)
	out, err = p.communicate()
	output=err+out
	output = output.decode("utf-8")

	return render_template('test.html', output = "<br/>".join(output.split("\n")))



if __name__ == "__main__":
    app.run(debug = True)
# ----------------------------------------
# end of main.py
# -----------------------------------------
